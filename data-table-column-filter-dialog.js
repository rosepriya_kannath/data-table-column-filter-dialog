Polymer({
  is: 'data-table-column-filter-dialog',

  properties: {
    gridSelector: {
      type: String
    },
    filterCount: Number,
    _comparisons: {
      type: Array,
      value: [{
        value: '>',
        forDate: 'After',
        forNumber: 'Greater than'
      }, {
        value: '<',
        forDate: 'Before',
        forNumber: 'Less than'
      }]
    }
  },
  behaviors: [Flytxt.webL10n],
  observers: ['_setUp(l10n)'],

  _setUp: function() {
    this.grid = document.querySelector(this.gridSelector);
    this.async(function() {
      var columns = this.grid.getContentChildren('[select=data-table-column]');
      this._resetFilterValue(columns);
      this.set('columns', columns);
    });
  },

  _applyFilters: function() {
    var filters = [];
    var me = this;
    var filterValues = [];
    _.forEach(me.columns, function(col) {
      filterValues.push(_.pick(col, ['filterValue' ,'filterBy', 'filterType', 'filterList', 'filterSource', 'idAccessor', 'labelAccessor', 'name']));
    });
    me.set('filterValues', _.cloneDeep(filterValues));
    for (var i = 0; i < this.columns.length; i++) {
      var column = this.columns[i];
      var clearedValues;
      if (column.filterValue) {
        clearedValues = column.filterValue.length - column.filterValue.filter(function(x) {
          return x === '';
        }).length;
      }
      if (!!column.filterType && column.filterValue.length > 0 && clearedValues !== 0) {
        var filter = {
          path: column.filterBy,
          type: column.filterType,
          filter: column.filterValue
        };
        filters.push(filter);
      }
    }
    this.grid.set('filter', filters);
    this.set('filterCount', filters.length);
    this.grid.set('selectedItems.filters', filters);
  },

  _resetFilterValue: function(columns) {
    columns.map(function(col) {
      if (col.filterBy) {
        col.filterValue = [];
      }
    });
  },

  _resetFilters: function() {
    // filter reset
    this.grid.set('filter', []);
    this.grid.set('selectedItems.filters', []);
    this.set('filterCount', 0);
    // form reset - workaround for form-reset, since initial values are not set in the form
    if (this.$.filterForm._customElementsInitialValues.indexOf(undefined) > -1) {
      var fields = this.$.filterForm._customElementsInitialValues.map(function(value) {
        return value === undefined ? null : value;
      });
      this.$.filterForm._customElementsInitialValues = fields;
    }
    //resetting selected values in list
    var list = this.querySelectorAll('.filterList');
    if (list.length) {
      _.each(list, function(l) {
        l.set('tagItems', []);
      });
    }
    this.$.filterForm.reset();
    this._resetFilterValue(this.columns);
  },
  _onNumberInput: function(e) {
    this.set('_numberPattern', '([' + e.target.value + ']\d*)?\d');
  },
  _checkFilterType: function(type) {
    return !!type;
  },
  
  _cancelFilters: function() {
    var me = this;
    me.columnsCopy = [];
    _.forEach(me.columns, function(col, i) {
      me.columnsCopy[i] = col.cloneNode(true);
      me.columnsCopy[i].filterValue = me.filterValues[i].filterValue;
      me.columnsCopy[i].filterBy = me.filterValues[i].filterBy;
      me.columnsCopy[i].filterType = me.filterValues[i].filterType;
      me.columnsCopy[i].filterList = me.filterValues[i].filterList;
      me.columnsCopy[i].filterSource = me.filterValues[i].filterSource;
      me.columnsCopy[i].idAccessor = me.filterValues[i].idAccessor;
      me.columnsCopy[i].labelAccessor = me.filterValues[i].labelAccessor;
      me.columnsCopy[i].name = me.filterValues[i].name;
    });
    this.set('columns', me.columnsCopy);
  },
  _openFilterDialog: function() {
    this.$.filterDialog.open();
    this.$.filterForm.firstElementChild.setAttribute('autofocus', true);
  }
});

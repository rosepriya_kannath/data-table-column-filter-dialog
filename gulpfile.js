'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var inlinesource = require('gulp-inline-source');
var bump = require('gulp-bump');
var src = ['data-table-column-filter-dialog.js',
           'data-table-column-filter-dialog.html'];

//Lint JavaScript
gulp.task('jshint', function () {
  return gulp.src(src)
    .pipe($.jshint.extract()) // Extract JS from .html files
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'));
});

//JavaScript Code Style
gulp.task('jscs', function () {
  return gulp.src([
      'data-table-column-filter-dialog.js'
    ])
    .pipe($.jscs());
});

gulp.task('inlinesource', function () {
  var options = {
    compress: false,
    handlers: [function(source, context, next) {
      if(source.type === 'css') {
        source.attributes.is = 'custom-style';
      }
      next();
    }]
  };
  return gulp.src('./src/app-login.html')
    .pipe(inlinesource(options))
    .pipe(gulp.dest('./'));
});

gulp.task('bump', function(){
  gulp.src('./bower.json')
  .pipe(bump())
  .pipe(gulp.dest('./'));
});

gulp.task('default', function (callback) {
  runSequence(
    ['jshint', 'jscs', 'inlinesource'],
    callback);
});

gulp.task('watch', function () {
  gulp.watch(src.concat('data-table-column-filter-dialog.css'), ['default']);
});